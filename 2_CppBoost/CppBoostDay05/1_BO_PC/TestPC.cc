#include "Producer.h"
#include "Consumer.h"
#include "TaskQueue.h"
#include "Thread.h"
#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::function;
using std::bind;

void test()
{
    TaskQueue  taskQue(10);//创建了唯一的TaskQueue对象
    Producer pro;
    Consumer con;
    Thread producer(bind(&Producer::produce, &pro, std::ref(taskQue)));
    Thread consumer(bind(&Consumer::consume, &con, std::ref(taskQue)));

    //生产者与消费者启动
    producer.start();
    consumer.start();

    //让主线程等待子线程的退出
    producer.stop();
    consumer.stop();
}

void test2()
{
    //对于锁而言，能不能进行复制与赋值呢?
    //条件变量，线程
    MutexLock mutex1;

    /* MutexLock mutex2 = mutex1;//复制,error */

    MutexLock mutex3;

    /* mutex3 = mutex1;//赋值,error */
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

