#include "Consumer.h"
#include "TaskQueue.h"
#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;

Consumer::Consumer()
{

}

Consumer::~Consumer()
{

}

void Consumer::consume(TaskQueue &taskQue)
{
    size_t cnt = 20;
    while(cnt--)
    {
        //消费数据
        int number = taskQue.pop();
        cout << ">>BO_PC.Consumer number = " << number << endl;
        sleep(1);
    }
}
