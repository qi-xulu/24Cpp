#include "Task.h"
#include "ThreadPool.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

class MyTask
: public Task
{
public:
    void process() override
    {
        //实现MyTask具体逻辑即可
        ::srand(::clock());
        int number = ::rand() % 100;
        cout << "number = " << number << endl;
    }
};

//1、现在的现象是：任务addTask可以添加20次，但是任务并没有执行
//20次，因为number的打印没有20次
//A:因为任务还没有执行完，在线程池的stop函数中将标志位_isExit
//设置为了true，导致在doTask函数中的while循环进不去了，也就是
//任务还没有执行完，就强制回收了工作线程
//
//2、现在已经可以确保任务执行完毕，但是线程池无法退出了
void test()
{
    unique_ptr<Task> ptask(new MyTask());
    ThreadPool pool(4, 10);

    //应该是先启动线程池中的工作线程，让工作线程等任务
    pool.start();

    size_t cnt = 20;
    while(cnt--)
    {
        pool.addTask(ptask.get());//智能指针向裸指针转换
        cout << "cnt = " << cnt << endl;
    }

    pool.stop();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

