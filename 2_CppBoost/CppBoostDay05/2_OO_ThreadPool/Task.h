#ifndef __TASK_H__
#define __TASK_H__

class Task
{
public:
    Task() = default;//使用C++11的语法，使用了默认构造函数
    virtual ~Task(){}

    virtual void process() = 0;
};

#endif
