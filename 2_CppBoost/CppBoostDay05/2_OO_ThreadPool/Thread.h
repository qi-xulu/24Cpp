#ifndef __THREAD_H__
#define __THREAD_H__

#include "NonCopyable.h"
#include <pthread.h>

class Thread
: NonCopyable
{
public:
    Thread();
    virtual ~Thread();

    //线程的开始
    void start();
    //线程的结束
    void stop();

private:
    //线程的入口函数
    static void *threadFunc(void *arg);
    //线程执行的任务
    virtual void run() = 0;

private:
    pthread_t _thid;//线程id
    bool _isRunning;//线程是否运行的标志
};

#endif

