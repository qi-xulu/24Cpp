#include "WorkThread.h"
#include "ThreadPool.h"

WorkThread::WorkThread(ThreadPool &pool)
: _pool(pool)
{
}

WorkThread::~WorkThread()
{

}

void WorkThread::run() 
{
    //这就是线程池交给工作线程做的任务
    //访问另外一个类的私有成员，可以使用友元
    _pool.doTask();
}
