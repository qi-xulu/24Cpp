#include "Thread.h"
#include <unistd.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

class MyThread
: public Thread
{
public:
    void run() override
    {
        //派生了的run方法具体做什么？
        while(1)
        {
            cout << "MyThread is running!!!" << endl;
            sleep(1);
        }
    }
};

void test2()
{
    MyThread mth;//栈对象
    mth.start();
    mth.stop();
}

void test()
{
    unique_ptr<Thread> pth(new MyThread());
    pth->start();
    pth->stop();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

