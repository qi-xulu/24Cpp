#include "ThreadPool.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <memory>
#include <functional>

using std::cout;
using std::endl;
using std::unique_ptr;
using std::bind;

class MyTask
{
public:
    void process() 
    {
        //实现MyTask具体逻辑即可
        ::srand(::clock());
        int number = ::rand() % 100;
        cout << "BO_ThreadPool.number = " << number << endl;
    }
};

void test()
{
    unique_ptr<MyTask> ptask(new MyTask());
    /* MyTask task; */
    ThreadPool pool(4, 10);

    //应该是先启动线程池中的工作线程，让工作线程等任务
    pool.start();

    size_t cnt = 20;
    while(cnt--)
    {
        pool.addTask(bind(&MyTask::process, ptask.get()));
        cout << "cnt = " << cnt << endl;
    }

    pool.stop();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

