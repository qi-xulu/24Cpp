#include <iostream>
#include <iterator>
#include <vector>
#include <list>
#include <set>
#include <algorithm>

using std::cout;
using std::endl;
using std::vector;
using std::copy;
using std::list;
using std::set;
using std::ostream_iterator;

using std::back_inserter;
using std::back_insert_iterator;
using std::front_inserter;
using std::front_insert_iterator;
using std::inserter;;
using std::insert_iterator;

void test()
{
    //将list中的元素插入到了vector尾部
    vector<int> vecNumber = {1, 3, 5, 7};
    list<int> listNumber = {11, 33, 77, 55, 44};
    /* copy(listNumber.begin(), listNumber.end(), */ 
    /*      back_inserter(vecNumber)); */
    copy(listNumber.begin(), listNumber.end(), 
         back_insert_iterator<vector<int>>(vecNumber));

    copy(vecNumber.begin(), vecNumber.end(), 
         ostream_iterator<int>(cout,  "  "));
    cout << endl;
    
    //将vector中的元素插入到了list头部
    cout << endl;
    /* copy(vecNumber.begin(), vecNumber.end(), */
    /*      front_inserter(listNumber)); */
    copy(vecNumber.begin(), vecNumber.end(),
         front_insert_iterator<list<int>>(listNumber));
    copy(listNumber.begin(), listNumber.end(), 
         ostream_iterator<int>(cout,  "  "));
    cout << endl;

    //将vector中的元素插入到set的任意位置
    cout << endl;
    set<int> setNumber = {1, 3, 8, 11, 100, 77, 55, 34};
    auto it = setNumber.begin();
    /* copy(vecNumber.begin(), vecNumber.end(), */
    /*      insert_iterator<set<int>>(setNumber, it)); */
    copy(vecNumber.begin(), vecNumber.end(),
         inserter(setNumber, it));
    copy(setNumber.begin(), setNumber.end(), 
         ostream_iterator<int>(cout,  "  "));
    cout << endl;
         
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

