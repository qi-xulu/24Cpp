#include <iostream>

using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    Point(const Point &rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "Point(const Point &)" << endl;
    }

    //this指针隐藏在(非静态)成员函数的第一个参数位置
    //this指针可以记录对象的地址
    //this指针是一个指针常量
    void print(/* Point * const this */)
    {
        /* this->_ix = 100;//ok */
        /* this = nullptr;//error,不能改变指向 */
        cout << "(" << this->_ix
             << ", " << this->_iy
             << ")" << endl;
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test()
{
    //为什么pt与pt2调用print的时候，打印结果没有交叉？
    //this
    Point pt(1, 2);
    cout << "pt = ";
    pt.print();

    cout << endl;
    Point pt2(3, 4);
    cout << "pt2 = ";
    pt2.print();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

