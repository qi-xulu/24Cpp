#include "Computer.h"
#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

//类中的成员函数是可以在类外面进行实现的
void Comupter::setBrand(const char *brand)
{
    strcpy(_brand, brand);//没有考虑内存越界
}

void Comupter::setPrice(float price)
{
    _price = price;
}

void Comupter::print()
{
    cout << "brand = " << _brand << endl
         << "price = " << _price << endl;
}
