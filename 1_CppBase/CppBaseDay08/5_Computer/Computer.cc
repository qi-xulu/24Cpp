#include "Computer.h"
#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

Comupter::Comupter(const char *brand, float price)
: _brand(new char[strlen(brand) + 1]())//申请了空间
, _price(price)
{
    cout << "Comupter(const char *, float)" << endl;
    /* _brand = new char[strlen(brand) + 1](); */
    strcpy(_brand, brand);
}

//类中的成员函数是可以在类外面进行实现的
void Comupter::setBrand(const char *brand)
{
    strcpy(_brand, brand);//没有考虑内存越界
}

void Comupter::setPrice(float price)
{
    _price = price;
}

void Comupter::print()
{
    if(_brand)
    {
        cout << "brand = " << _brand << endl;
    }
    cout << "price = " << _price << endl;
}

//编译器生成的默认析构函数有些时候是没有作用的
//特别是对于堆空间的形式：需要手动的进行数据成员的回收
Comupter::~Comupter()
{
    cout << "~Comupter()" << endl;
    if(_brand)
    {
        delete [] _brand;
        _brand = nullptr;
    }
}
