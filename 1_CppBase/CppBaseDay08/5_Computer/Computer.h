#ifndef __COMPUTER_H__
#define __COMPUTER_H__

class Comupter 
{
public:    
    Comupter(const char *brand, float price);
    void setBrand(const char *brand);
    void setPrice(float price);
    void print();
    ~Comupter();

private:
    char *_brand;
    float _price;
};

#endif

