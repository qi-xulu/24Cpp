#include <iostream>
#include <list>
#include <vector>

using std::cout;
using std::endl;
using std::list;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    list<int> number = {1, 2, 4, 6, 8, 9, 3, 6, 7};
    display(number);

    cout << endl << "在list尾部进行插入与删除" << endl;
    number.push_back(11);
    number.push_back(22);
    display(number);
    number.pop_back();
    display(number);

    cout << endl << "在list头部进行插入与删除" << endl;
    number.push_front(100);
    number.push_front(200);
    display(number);
    number.pop_front();
    display(number);

    cout << endl << "在list的任意位置进行插入" << endl;
    auto it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 33);
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl;
    number.insert(it, 3, 666);
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl;
    vector<int> vec = {200, 500, 600, 300};
    number.insert(it, vec.begin(), vec.end());
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl;
    number.insert(it, {123, 456, 789});
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl << "清空list中的元素" << endl;
    number.clear();//清空所有的元素
    /* number.shrink_to_fit();//回收多余的空间,error */
    cout << "size() = " << number.size() << endl;
    /* cout << "capacity() = " << number.capacity() << endl;//error */

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

