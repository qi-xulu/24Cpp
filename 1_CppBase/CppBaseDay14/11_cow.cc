#include <iostream>

using std::cout;
using std::endl;

//引用计数到底存在内存的那个地方
//1、全局区（静态区） 不行
//2、栈上 不行
//3、堆上 放在堆的任意位置都可以 
//4、文字常量区 不行
//5、程序代码区 不行
class String
{
public:
    String();
    String(const String &rhs)
    : _pstr(rhs._pstr)
    , _refCount(rhs._refCount)
    {
        /* rhs._refCount++;//error */
        _refCount++;
    }
private:
    char *_pstr;
    int _refCount;
};
void test()
{
    String s1;//1
    String s2 = s1;//2

    String s3;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

