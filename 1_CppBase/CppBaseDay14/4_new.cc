#include <iostream>

using std::cout;
using std::endl;

void test()
{
    int *pInt = new int(10);

    delete pInt;
    pInt = nullptr;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

