#include <stdlib.h>
#include <iostream>

using std::cout;
using std::endl;

//单例模式的自动释放第三种形式：饿汉模式 +atexit
//解决多线程不安全的问题
class Singleton
{
public:
    static Singleton *getInstance()
    {
        if(nullptr == _pInstance)
        {
            _pInstance = new Singleton();
            atexit(destroy);
        }
        return _pInstance;
    }

    static void destroy()
    {
        if(_pInstance)
        {
            delete _pInstance;
            _pInstance = nullptr;
        }
    }

private:
    Singleton()
    {
        cout <<"Singleton()" << endl;
    }

    ~Singleton()
    {
        cout <<"~Singleton()" << endl;
    }

private:
    static Singleton *_pInstance;
};

//饱汉(懒汉)模式
/* Singleton *Singleton::_pInstance = nullptr; */

//饿汉模式
Singleton *Singleton::_pInstance = Singleton::getInstance();

void test()
{
    Singleton::getInstance();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

