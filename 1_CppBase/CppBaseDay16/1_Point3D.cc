#include <iostream>

using std::cout;
using std::endl;

//1、不管什么继承方式，都不能在派生类类体中访问基类中的私有成员
//2、不管什么继承方式，在派生类类体中，除了基类的私有成员不能访问
//外，其他的都可以访问
//3、对于派生类的对象而言，只能访问公有继承基类中的公有成员，其他
//的都不能访问

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    int getY() const
    {
        return _iy;
    }

    void print()
    {
        cout << "_ix = " << _ix << endl
             << "_iy = " << _iy << endl;
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
protected://留给儿子访问的
    int _ix;
private://私有的，封装性
    int _iy;
};

class Point3D 
: public Point
{
public:
    Point3D(int ix = 0, int iy = 0, int iz = 0)
    : Point(ix, iy)//为了初始化从基类继承过来的数据成员，可以借助基类的构造函数
    , _iz(iz)
    {
        cout << "Point3D(int = 0, int = 0, iny = 0)" << endl;
    }

    void print3D()
    {
        /* print(); */
        cout << "_ix = " << _ix << endl//protected
             /* << "_iy = " << _iy << endl; */ 
             << "_iy = " << getY() << endl//public
             << "_iz = " << _iz << endl;//private
    }

    ~Point3D()
    {
        cout << "~Point3D()" << endl;
    }
private:
    int _iz;
};

//能不能被继承与可不可以直接访问是等价的吗?
//可以继承但是不一定可以访问，因为还有private的特点
void test()
{
    cout << "sizeof(Point) = " << sizeof(Point) << endl;
    cout << "sizeof(Point3D) = " << sizeof(Point3D) << endl;

    Point3D pt3d(1, 2, 3);
    cout << "pt3d = ";
    cout << "pt3d.getY() = " << pt3d.getY() << endl;
    /* pt3d._ix;//error,protected */
    /* pt3d._iy;//error,private */
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

