#include <iostream>

using std::cout;
using std::endl;

class NonCopyable
{
public:
    NonCopyable()
    {}

    NonCopyable(const NonCopyable &rhs) = delete;
    NonCopyable &operator=(const NonCopyable &rhs) = delete;
};

class Example
: NonCopyable
{
public:
    Example()
    {
        cout <<"Example()" << endl;
    }

    ~Example()
    {
        cout <<"~Example()" << endl;
    }
};

void test()
{
    Example ex1;
    Example ex2 = ex1;//拷贝构造函数,error

    cout << endl;
    Example ex3;
    ex3 = ex1;//赋值运算符函数,error
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

