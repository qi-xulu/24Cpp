#include <iostream>
#include <string>
#include <limits>

using std::cout;
using std::endl;
using std::cin;
using std::string;

void printStreamStatus()
{
    cout << "cin.badbit = " << cin.bad() << endl;
    cout << "cin.failbit = " << cin.fail() << endl;
    cout << "cin.eofbit = " << cin.eof() << endl;
    cout << "cin.goodbit = " << cin.good() << endl;
}
void test()
{
    int number = 0;
    printStreamStatus();
    cin >> number;
    printStreamStatus();

    cout << "number = " << number << endl;
    cin.clear();//重置流的状态
    //清空缓冲区
    /* cin.ignore(1024, '\n'); */
    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    printStreamStatus();

    string s1;
    cin >> s1;
    cout << "s1 = " << s1 << endl;
}

void test2()
{
    //循环操作
    int number = 0;
    //逗号表达式
    //ctrl+ d可以退出
    while(cin >> number, !cin.eof())//真值表达式
    {
        if(cin.bad())
        {
            cout << "stream is bad" << endl;
            return;
        }
        else if(cin.fail())
        {
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            cout << "pls input int data" << endl;
        }
        else 
        {
            cout << "number = " << number << endl;
        }
    }
}

int main(int argc, char *argv[])
{
    test2();
    return 0;
}

