#include <iostream>
#include <set>
#include <utility>
#include <string>

using std::cout;
using std::endl;
using std::set;
using std::pair;
using std::string;

void test()
{
    //vector<int> vec;
    //set的特征(去重、排序)
    //1、存放key类型的元素，key值是唯一的,不能重复
    //2、默认情况会按照key值进行升序排列
    set<int> number = {1, 3, 7, 9, 3, 5, 7};

    /* set<int>::iterator it; */
    for(auto it = number.begin(); it != number.end(); ++it)
    {
        cout << *it << "  ";
    }
    cout << endl;

    cout << endl;
    size_t cnt = number.count(5);
    cout <<"cnt = " << cnt << endl;

     set<int>::iterator it = number.find(17);
     if(it == number.end())
     {
         cout << "该元素不在set中" << endl;
     }
     else
     {
         cout << "该元素存在set中 " << *it << endl;
     }

     cout << endl;
     pair<set<int>::iterator, bool> ret = number.insert(8);
     if(ret.second)
     {
         cout << "插入成功 " << *ret.first << endl;
     }
     else
     {
         cout << "插入失败，因为该元素已经存在set中" << endl;
     }
     
     for(auto it = number.begin(); it != number.end(); ++it)
     {
         cout << *it << "  ";
     }
     cout << endl;

     cout << endl;
     //set不支持下标访问运算符
     /* cout << "number[0] = " << number[0] << endl;//error */
     auto it2 = number.begin();
     cout << "*it2 = " << *it2 << endl;
     /* *it2 = 100;//error */
}

void test2()
{
    pair<int, string> number = {1, "hello"};
    cout << number.first << "    " << number.second << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

