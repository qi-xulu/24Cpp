#include <iostream>

using std::cout;
using std::endl;

void print()
{
    cout << endl;
}

//...位于Args或者...位于args的时候称为打包
template <typename T, typename ...Args>//模板参数包
void print(T t1, Args ...args)//函数参数包
{
    cout << t1 << "  ";

    //...位于args的右边的时候称为解包
    print(args...);//递归的调用,
}


void test()
{
    //只有一个参数，所以只会走无参print()
    print();
    //有多于一个参数
    //cout << 1 <<" ";
    //print("hello")
    //   cout << "hello" << "  ";
    //   print();
    //       cout << endl;
    print(1, "hello");
    //多于一个参数
    //cout << "hello" << " ";
    //print(3.3, true);
    //    cout << 3.3 << " ";
    //    print(true);
    //       cout << true << " ";
    //       print();
    //          cout << endl;
    print("hello", 3.3, true);
    print("hello", 3.3, true, 4.4, 5.5, "world");
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

