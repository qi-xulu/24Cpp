#include <iostream>
#include <algorithm>
#include <vector>
#include <functional>

using std::cout;
using std::endl;
using std::for_each;
using std::vector;
using std::copy;
using std::remove_if;
using std::bind1st;
using std::bind2nd;

void func(int &value)
{
    cout << value << "  ";
}

void test()
{
    vector<int> vec = {1, 3, 5, 7, 8, 9, 4, 3, 2};
    for_each(vec.begin(), vec.end(), func);
    cout << endl;

    cout << endl;
    //一元断言/谓词
    /* auto it = remove_if(vec.begin(), vec.end(), */ 
    /*                     bind2nd(std::greater<int>(), 5)); */
    auto it = remove_if(vec.begin(), vec.end(), 
                        bind1st(std::less<int>(), 5));
    vec.erase(it, vec.end());

    for_each(vec.begin(), vec.end(), func);
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

