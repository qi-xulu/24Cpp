#include <iostream>
#include <log4cpp/BasicLayout.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/Category.hh>
#include<log4cpp/Priority.hh>

using std::cout;
using std::endl;
using namespace log4cpp;

void test()
{
    //日志的格式
    BasicLayout *pbl = new BasicLayout();
    
    //日志的目的地
    OstreamAppender *poa = 
        new OstreamAppender("OstreamAppender", &cout);
    poa->setLayout(pbl);

    //日志记录器
    Category &root = Category::getRoot();
    root.addAppender(poa);

    //日志的过滤器
    root.setPriority(Priority::ERROR);

    root.emerg("This is an emerg message");
    root.fatal("This is an fatal message");
    root.alert("This is an alert essage");
    root.crit("This is an crit message");
    root.error("This is an error message");
    root.notice("This is an notice message");
    root.debug("This is an debug message");
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

