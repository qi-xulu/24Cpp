#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::cerr;
using std::ofstream;
using std::ifstream;
using std::string;

void test()
{
    //对于文件输出流而言，当文件不存在的时候，就创建文件；
    //当文件存在的时候,就会清空文件中的内容
    ofstream ofs("test.txt");
    if(!ofs.good())
    {
        cerr << "ofstream is not good" << endl;
        return;
    }

    ofs << "wangdao" << endl;

    ofs.close();
}

void test2()
{
    //想将wd.txt中的内容写入到test.txt中
    ifstream ifs("wd.txt");
    if(!ifs.good())
    {
        cerr << "ifstream is not good" << endl;
        return;
    }

    //对于文件输出流而言，当文件不存在的时候，就创建文件；
    //当文件存在的时候,就会清空文件中的内容
    ofstream ofs("test.txt");
    if(!ofs.good())
    {
        cerr << "ofstream is not good" << endl;
        return;
    }

    string line;
    while(getline(ifs, line))
    {
        ofs << line << endl;
    }

    ifs.close();
    ofs.close();
}
int main(int argc, char *argv[])
{
    test2();
    return 0;
}

