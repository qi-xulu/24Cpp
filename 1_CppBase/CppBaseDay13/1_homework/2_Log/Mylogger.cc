#include "Mylogger.h"
#include <log4cpp/PatternLayout.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/Priority.hh>

using namespace log4cpp;

Mylogger *Mylogger::_pInstance = nullptr;

Mylogger *Mylogger::getInstance()
{
    if(nullptr == _pInstance)
    {
        _pInstance = new Mylogger();
    }

    return _pInstance;
}

void Mylogger::destroy()
{
    if(_pInstance)
    {
        delete [] _pInstance;
        _pInstance = nullptr;
    }
}

void Mylogger::warn(const char *msg)
{
    _root.warn(msg);
}

void Mylogger::error(const char *msg)
{
    _root.error(msg);
}

void Mylogger::debug(const char *msg)
{
    _root.debug(msg);
}

void Mylogger::info(const char *msg)
{
    _root.info(msg);
}

Mylogger::Mylogger()
: _root(Category::getRoot().getInstance("MyCat"))
{
    cout << "Mylogger::Mylogger()" << endl;
    //日志的格式
    PatternLayout *ppl = new PatternLayout();
    ppl->setConversionPattern("%d [%p] %c %m %n");

    PatternLayout *ppl2 = new PatternLayout();
    ppl2->setConversionPattern("%d [%p] %c %m %n");

    //日志的目的地
    OstreamAppender *poa = 
        new OstreamAppender("OstreamAppender", &cout);
    poa->setLayout(ppl);

    FileAppender *pfl = 
        new FileAppender("FileAppender", "wd.txt");
    pfl->setLayout(ppl2);

    //添加日志的目的地
    _root.addAppender(poa);
    _root.addAppender(pfl);

    //日志的优先级
    _root.setPriority(Priority::DEBUG);
}

Mylogger::~Mylogger()
{
    cout << "Mylogger::~Mylogger()" << endl;
    Category::shutdown();
}
