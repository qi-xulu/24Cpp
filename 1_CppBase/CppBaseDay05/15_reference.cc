#include <iostream>

using std::cout;
using std::endl;

//提出引用就是为了减少指针的使用
//1、引用作为函数的参数
#if 0
void swap(int x, int y)//int x = a, int y = b值传递 = 值的拷贝
{
    int tmp = x;
    x = y;
    y = tmp;
}
#endif
//参数传递指针的形式:地址传递(值传递)
#if 0
void swap(int *px, int *py)//int *px = &a, int *py = &b
{
    int tmp = *px;
    *px = *py;
    *py = tmp;
}
#endif

//引用传递:简单直接，效率还比较高
void swap(int &x, int &y)//int &x = a, int &y = b
{
    int tmp = x;
    x = y;
    y = tmp;
}
void test()
{
    int a = 3, b = 4;
    cout << "交换之前a = " << a << ", b = " << b << endl;
    swap(a, b);
    cout << "交换之后a = " << a << ", b = " << b << endl;
}

//2、引用作为函数的返回类型
int main(int argc, char *argv[])
{
    test();
    return 0;
}

