#include <iostream>

//3、using声明机制（推荐使用这种）
//一次只引出命名空间中的一个实体（要用哪一个就引出哪一个）
//可以解决作用域限定符的缺陷，使用比较麻烦；
//可以解决using编译指令的冲突问题
using std::cout;
using std::endl;

#if 0
int cout(int x, int y)
{
    //作用域限定符的形式
    std::cout << "x = " << x << ", y = " << y << std::endl;
}
#endif

int main(int argc, char *argv[])
{
    int a = 3, b = 4;
    /* cout(a, b); */
    return 0;
}

