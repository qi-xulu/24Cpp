#include <iostream>


//2、作用域限定符的形式
//优点：即使自己定义的实体与命名空间std中的实体一样
//也没有问题
//
//缺点：
//每一次使用std中的实体的时候，需要带上std::，写起来就比较麻烦
int cout(int x, int y)
{
    //作用域限定符的形式
    std::cout << "x = " << x << ", y = " << y << std::endl;
}

int main(int argc, char *argv[])
{
    int a = 3, b = 4;
    cout(a, b);
    return 0;
}

