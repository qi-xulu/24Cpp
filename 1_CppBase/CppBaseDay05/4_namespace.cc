#include <iostream>

//std是标准命名空间的名字
using namespace std;//1、using编译指令

//using编译指令的缺点
//1、我们不能完全知道std中有哪些实体，所以可能自己再去定义
//的时候，定义的函数的名字、变量的名字、结构体的名字与
//名字空间中的冲突
//
//优点：
//可以一次将标准命名空间std中的实体全部都引出来，就更加方便
//快捷
int cout()
{
}

int main(int argc, char *argv[])
{
    cout();
    return 0;
}

