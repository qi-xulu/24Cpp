#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

void test()
{
    //C风格字符串是可以直接转换为C++风格字符串的
    //C++       C
    string s1 = "hello";
    string s2 = "world";
    cout << "s1 = " << s1 << endl;
    cout << "s2 = " << s2 << endl;
    
    //C++风格字符串是可以转换为C风格字符串的
    cout << endl;
    const char *pstr = s1.c_str();
    cout << "pstr = " << pstr << endl;

    cout << endl;
    //获取字符串的长度
    size_t len1 = s1.size();
    size_t len2 = s2.length();
    cout << "len1 = " << len1 << endl;
    cout << "len2 = " << len2 << endl;

    cout << endl;
    //字符串的拼接
    string s3 = s1 + s2;
    cout << "s1 = " << s1 << endl;
    cout << "s2 = " << s2 << endl;
    cout << "s3 = " << s3 << endl;

    cout << endl;
    string s4 = s3 + "wangdao";
    cout << "s4 = " << s4 << endl;

    cout << endl;
    //字符串中字符的修改
    s4[0] = 'H';
    cout << "s4 = " << s4 << endl;

    cout << endl;
    string s5 = s4 + 'K';
    cout << "s5 = " << s5 << endl;

    cout << endl;
    string s6 = "wuhan" + s5 + 'K';
    cout << "s6 = " << s6 << endl;

    cout << endl;
    for(size_t idx = 0; idx != s6.size(); ++idx)
    {
        cout << s6[idx] << "   ";
    }
    cout << endl;

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

