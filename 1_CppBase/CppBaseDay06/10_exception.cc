#include <iostream>

using std::cout;
using std::endl;
using std::cin;

void test()
{
    int x, y;
    cin >> x >> y;

    try
    {
        if(0 == y)//编码习惯
        /* if(y == 0) */
        /* if(y = 0) */
        /* if(0 = y) */
        {
            throw y;//抛出异常
        }
        else
        {
            cout << "x/y = " << x/y << endl;
        }
    }
    catch(double e)
    {
        cout << "catch(double)" << endl;
    }
    catch(int e)
    {
        cout << "catch(int)" << endl;
    }

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

