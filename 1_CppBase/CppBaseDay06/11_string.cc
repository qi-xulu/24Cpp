#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

void test()
{
    //字符数组
    //C的字符串是以'\0'结尾的
    char str1[] = "hello";
    char str2[] = {'w', 'o', 'r', 'l', 'd'};

    /* cout << "strlen(str1) = " << strlen(str1) << endl; */
    /* cout << "strlen(str2) = " << strlen(str2) << endl; */
    cout << "sizeof(str1) = " << sizeof(str1) << endl;
    cout << "sizeof(str2) = " << sizeof(str2) << endl;

    //字符指针
    char *pstr = "hello,world";
    cout << "strlen(pstr) = " << strlen(pstr) << endl;
    //sizeof测试指针的时候，不能将指针所指的内容长度
    //计算出来,只能计算指针的大小
    //对于32位系统而言，一个指针是4字节，
    //对于64位系统而言，一个指针是8字节，
    cout << "sizeof(pstr) = " << sizeof(pstr) << endl;
    /* cout << "*pstr = " << *pstr << endl; */
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

