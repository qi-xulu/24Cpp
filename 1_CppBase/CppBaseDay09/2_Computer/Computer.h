#ifndef __COMPUTER_H__
#define __COMPUTER_H__

class Comupter 
{
public:    
    Comupter(const char *brand, float price);
    Comupter(const Comupter &rhs);
    Comupter &operator=(const Comupter &rhs);
    void setBrand(const char *brand);
    void setPrice(float price);

    //const版本的成员函数可以与非const版本的成员函数形成重载
    //const版本的成员函数的目的是不修改数据成员，为了保护数据
    //成员不被修改
    //如果只想写一个版本的成员函数，建议写const版本的
    void print();
    void print() const;

    ~Comupter();

    static void printTotalPrice();

private:
    char *_brand;
    float _price;
    //静态数据成员不占用类中对象的大小，静态数据成员是被
    //类创建的对象所共享的,位于全局/静态区
    static float _totalPrice;
};

#endif

