#include <iostream>

using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    Point(const Point &rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "Point(const Point &)" << endl;
    }

    //1、参数中的引用可以去掉吗？
    //A:可以删掉，但是会多执行一次拷贝构造函数；为了避免多执行
    //一次拷贝构造函数，所以一般都没有删掉引用符号,会增加函数
    //调用开销
    
    //2、参数中的const可以去掉吗？
    //A:不能删掉，不然会出现非const左值引用不能识别右值
    //也就是赋值符号的右边不能是右值pt = Point(1, 2)
    //
    //3、返回类型的引用符号可以去掉吗？
    //A:不能删掉，如果删掉的话会多执行一次拷贝构造函数，
    //会增加函数调用的开销（与参数中的引用原理一样）
    //
    //4、返回类型可以不是Point吗？
    //A:如果返回类型不是Point，那么就不支持连等
    //pt3 = pt2 = pt
    Point &operator=(const Point &rhs)
    {
        cout << "Point &operator=(const Point &)" << endl;
        _ix = rhs._ix;//赋值
        _iy = rhs._iy;//赋值

        return *this;
    }

    void print()
    {
        cout << "(" << this->_ix
             << ", " << this->_iy
             << ")" << endl;
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test()
{
    Point pt(1, 2);
    cout << "pt = ";
    pt.print();

    cout << endl;
    Point pt2(3, 4);
    cout << "pt2 = ";
    pt2.print();

    cout << endl;
    Point pt4(5, 6);
    cout << "pt4 = ";
    pt4.print();

    cout << endl;
    pt2 = pt;//赋值
    /* pt2.operator=(pt);//赋值运算符函数 */
    cout << "pt2 = ";
    pt2.print();

    cout << endl;
    /* pt2 = Point(1, 4); */

    int a = 10;
    int b = 20;
    int c = 30;
    c = b = a;

    //Point void
    pt4 = pt2 = pt;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

