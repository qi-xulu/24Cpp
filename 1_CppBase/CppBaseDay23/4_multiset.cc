#include <iostream>
#include <set>
#include <utility>
#include <vector>

using std::cout;
using std::endl;
using std::multiset;
using std::pair;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    //multiset的特征：
    //1、存放的是key类型，key值是不唯一的，可以重复
    //2、默认情况下，会按照key值进行升序排列
    //3、multiset的底层使用的是红黑树结构
    multiset<int> number{1, 3, 5, 8, 9, 7, 3, 6, 5, 2};
    /* multiset<int, std::greater<int>> number{1, 3, 5, 8, 9, 7, 3, 6, 5, 2}; */
    display(number);

    cout << endl << "multiset的查找" << endl;
    size_t cnt = number.count(3);
    cout << "cnt = " << cnt << endl;

    multiset<int>::iterator it = number.find(4);
    if(it != number.end())
    {
        cout << "该元素在multiset中" << *it << endl;
    }
    else
    {
        cout << "该元素不在multiset中" << endl;
    }

    cout << endl << "测试bound函数" << endl;
    auto it2 = number.lower_bound(5);
    cout << "*it2 = " << *it2 << endl;

    auto it3 = number.upper_bound(5);
    cout << "*it3 = " << *it3 << endl;

    pair<multiset<int>::iterator, multiset<int>::iterator> 
        ret = number.equal_range(5);
    while(ret.first != ret.second)
    {
        cout << *ret.first << "  ";
        ++ret.first;
    }
    cout << endl;

    cout << endl << "insert操作" << endl;
    number.insert(10);
    display(number);

    cout << endl;
    vector<int> vec = {1, 3, 5, 9, 11, 34, 67, 4, 2};
    number.insert(vec.begin(), vec.end());
    display(number);

    cout << endl;
    number.insert({12, 88, 33});
    display(number);

    cout << endl << "删除操作" << endl;
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.erase(it);
    display(number);

    cout << endl << "下标访问" << endl;
    /* cout << "number[1] = " << number[1] << endl; */

    cout << endl << "修改操作" << endl;
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    /* *it = 100;//error */
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

